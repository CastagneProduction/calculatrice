﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class Calculatrice : Window
    {
        string Operator = "";
        string[] numbers = { "", "" };
        int etape = 0;

        public Calculatrice()
        {
            InitializeComponent();
        }
        
        private void On_Click_Number(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            numbers[etape] += btn.Content.ToString();
            Affichage.Content = numbers[etape];
        }

        private void On_Click_Operator(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            string cont = btn.Content.ToString();
            if (cont != "=")
            {
                Operator = cont;
                etape = 1;
                Affichage.Content = "";
            }
            else
            {
                double result=0;
                etape = 0;
                switch (Operator)
                {
                    case "+": result = Convert.ToDouble(numbers[0]) + Convert.ToDouble(numbers[1]);
                        break;
                    case "-": result = Convert.ToDouble(numbers[0]) - Convert.ToDouble(numbers[1]);
                        break;
                    case "*": result = Convert.ToDouble(numbers[0]) * Convert.ToDouble(numbers[1]);
                        break;
                    case "/": if (Convert.ToDouble(numbers[1]) != 0) result = Convert.ToDouble(numbers[0]) / Convert.ToDouble(numbers[1]);
                        break;
                    case "%": result = Convert.ToDouble(numbers[0]) % Convert.ToDouble(numbers[1]);
                        break;
                    case "^":
                        result = Math.Pow(Convert.ToDouble(numbers[0]), Convert.ToDouble(numbers[1]));
                        break;
                }
                numbers[0] = result.ToString();
                numbers[1] = "";
                Affichage.Content = numbers[0];
                etape = 0;
            }
        }
    }
}
